from networkx.drawing.nx_agraph import graphviz_layout
from collections import defaultdict
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np


def loc(word):
    return [letter for letter in word]


def get_adj_matrix(matrix):
    adj = defaultdict(lambda: [])

    for i, letter in enumerate(matrix[:, -1].tolist()):
        if letter != "$":
            adj[letter].append(i)

    adj = {k: v for k, v in sorted(dict(adj).items(), key=lambda item: item[0])}

    return adj


def get_strings_starting_with(table, string):

    if string == "":
        return 0, len(table[0])

    candidates = []
    string = string[::-1]

    for i, row in enumerate(table):
        if row.startswith(string):
            candidates.append(i)

    return candidates[0], candidates[-1]


class PT:

    def __init__(self, reference):
        self.__reference = reference
        self.__table = self.__matrix = None
        self.__rotate()
        self.__index = -1
        self.__graph = nx.DiGraph()
        self.__nodes = []
        self.__adjacencies = {}

    def __rotate(self):
        rotations = [self.__reference + "$"]

        for i in range(1, len(self.__reference) + 1):
            rotations.append(rotations[i - 1][1:] + rotations[i - 1][0])

        rotations = sorted(rotations)

        self.__table = [word for word in rotations]
        self.__matrix = np.asarray([loc(word) for word in rotations])

        return self

    def build_trie_recursive(self, matrix, nodes, tot_adjacencies, curr_letter="", string="", level=0, span=0):

        string += curr_letter

        if curr_letter == "$":
            return

        adj = get_adj_matrix(matrix)

        node = {"level": level,
                "span": span,
                "index": self.__index,
                "letter": curr_letter,
                "range": get_strings_starting_with(self.__table, string)}

        tot_adjacencies[node["index"]] = []
        nodes.append(node)

        level += 1
        for i, (letter, adjacencies) in enumerate(adj.items()):
            self.__index += 1
            next_node = {"level": level,
                         "span": i,
                         "index": self.__index,
                         "letter": letter}

            tot_adjacencies[node["index"]].append(next_node)

            self.build_trie_recursive(matrix[adjacencies, :-1], nodes, tot_adjacencies,
                                      curr_letter=letter, string=string, level=level, span=i,)

        return

    def build_trie(self, draw=False):
        self.build_trie_recursive(self.__matrix, self.__nodes, self.__adjacencies)
        if draw:
            self.__draw()

        return self.__nodes, self.__adjacencies

    def __draw(self):

        labels = {}
        colors = ["#42c5f5"]*len(self.__nodes)
        edges_labels = {}

        for i, node in enumerate(self.__nodes):
            self.__graph.add_node(node["index"])
            labels[node["index"]] = node["range"]

        for node_from, nodes_to in self.__adjacencies.items():
            for node_to in nodes_to:
                self.__graph.add_edge(node_from, node_to["index"])
                edges_labels[(node_from, node_to["index"])] = node_to["letter"]

        pos = graphviz_layout(self.__graph, prog='dot')

        plt.title('Prefix Trie')
        plt.figure(figsize=(20, 20))
        nx.draw(self.__graph, pos, labels=labels, node_color=colors, arrows=True)
        nx.draw_networkx_edge_labels(self.__graph, pos, edge_labels=edges_labels, rotate=False)
        plt.show()
