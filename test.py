import prefixtrie.prefixtrie as pt

if __name__ == "__main__":
    nodes, adjacencies = pt.PT('CATGCTACTGACTA').build_trie(draw=True)
